# Firefox 36+ Password & Cookie Recovery #

[Original article](http://hackforums.net/showthread.php?tid=4793796), [Documentation browser](http://raidersec.blogspot.com/2013/06/how-browsers-store-your-passwords-and.html)

**Pastebin:**

[SQLiteHandler](http://pastebin.com/HJtbm00p), [Firefox](http://pastebin.com/QnrxxQYn)

**Summary:**

Will recover cookies (stored in sqlite3) and decrypt passwords stored in the logins.json.
 
**Tested with:** 

win7, 8.1 x64 (compiled as x86) and Firefox 37.0.2 (also tested briefly on 36.0)

**Required Libraries:**

Newtonsoft.Json (.net 2.0 one works fine)
 
**Required References:**

Microsoft.VisualBasic (for the SQLiteHandler)

Newtonsoft.Json (for the easy deserializing)

System (well, duh)
 

```
#!c#

List<FirefoxCookie> firefoxCookies = Firefox.Cookies();
List<FirefoxPassword> firefoxPasswords = Firefox.Passwords();
```